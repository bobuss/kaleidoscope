
function Point(x, y) {
  this.x = x || 0;
  this.y = y || 0;
}

Point.prototype = {
  subtract: function(v) {
    return new Point(this.x - v.x, this.y - v.y);
  },
  multiply: function(v) {
    return new Point(this.x * v, this.y * v);
  },
  dot: function(v) {
    return this.x * v.x + this.y * v.y;
  },
  distance: function(other) {
    return this.subtract(other).length();
  },
  length: function() {
    return Math.sqrt(this.dot(this));
  },
  toAngles: function() {
    return {
      theta: Math.atan2(0, this.x),
      phi: Math.asin(this.y / this.length())
    };
  },
};

Point.fromAngles = function(theta, phi) {
  return new Point(Math.cos(theta) * Math.cos(phi), Math.sin(phi), Math.sin(theta) * Math.cos(phi));
};



clearScreen = function() {
  context.fillStyle = 'rgba(0, 0, 0, 1)';
  context.fillRect(0, 0, width, height);
}

drawPoint = function(point, color) {
  context.fillStyle = color;
  context.beginPath();
  context.arc(point.x + center.x, point.y + center.y, 2, 0, 100);
  context.fill();
}

drawProjections = function(point) {
  var centerDistance = point.distance(center),
      color = tinycolor(colorBase.toString()).spin(centerDistance).toRgbString(),
      angles = point.toAngles(),
      i;

  for (i = 0; i < nbProjections ; i++ ) {
    var projection = Point.fromAngles(
      angles['theta'],
      angles['phi'] + 2 * i * Math.PI/nbProjections
    ).multiply(point.length());

    drawPoint(projection, color);

    if (symetries) {
      drawPoint(new Point(projection.x, -projection.y), color);
    }
  }
}

var width = 600,
    height = 600,
    canvas = document.getElementById("kaleidoscope"),
    context = canvas.getContext("2d"),
    center = new Point(width/2, height/2),
    points = [],
    nbProjections = 5,
    symetries = true,
    colorBase = tinycolor({ r: 240, g: 120, b: 80 }),
    isDrawing = false;

canvas.setAttribute("width", width);
canvas.setAttribute("height", height);
context.fillStyle = 'rgba(0, 0, 0)';
context.fillRect(0, 0, width, height);

canvas.addEventListener("mousemove", function(e) {
  if (isDrawing) {
    var point = new Point(e.offsetX, e.offsetY).subtract(center);
    points.push(point);
    drawProjections(point);
  }
});

canvas.addEventListener("mousedown", function(e) {
  isDrawing = true;
  canvas.style.cursor = "crosshair";
});

canvas.addEventListener("mouseout", function(e) {
  isDrawing = false;
  canvas.style.cursor = "default";
});

canvas.addEventListener("mouseup", function(e) {
  isDrawing = false;
  canvas.style.cursor = "default";
});

document.getElementById("nb").addEventListener("change", function(e) {
  nbProjections = e.target.valueAsNumber;
  clearScreen();
  points.forEach(function(point) {
    drawProjections(point);
  });
  return false;
});

document.getElementById("sym").addEventListener("change", function(e) {
  symetries = e.target.checked;
  clearScreen();
  points.forEach(function(point) {
    drawProjections(point);
  });
  return false;
});

document.getElementById("clean").addEventListener("click", function() {
  points = [];
  clearScreen();
  return false;
})
